package com.scorpionglitch.gmzuul.filters;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

public class GiantBombRouteFilter extends ZuulFilter {
	private RequestContext context;
	private HttpServletRequest request;
	private String requestURI;
	private String requestMethod;
	
	@Override
	public boolean shouldFilter() {
		context = RequestContext.getCurrentContext();
		request = context.getRequest();
		
		requestURI = request.getRequestURI();
		requestMethod = request.getMethod();
		
		boolean isGiantBombRequest = requestURI.startsWith("/giantbomb/");
		boolean isGetMethod = requestMethod.compareToIgnoreCase("GET") == 0;
		
		System.out.println(isGiantBombRequest + " " + isGetMethod);
		
		return  isGiantBombRequest && isGetMethod;
	}

	
	@Override
	public Object run() throws ZuulException {
		System.out.println("Giantbomb RouteFilter: (" + requestMethod + ") " + requestURI);
		
		Enumeration<String> enumeration = request.getHeaderNames();
				
		String header;
		while (enumeration.hasMoreElements()) {
			header = enumeration.nextElement();
			System.out.println(header + ":" + request.getHeader(header));
		}
		
		return null;
	}

	@Override
	public String filterType() {
		/*
		 * Return a string of "pre", "route", "post", or "error".
		 * Zuul will parse the string to figure out what type of filter
		 * this class should represent.
		 * pre-	executes before the request is routed to the proper service
		 * route- doctors the URL/routing information/destination
		 * post- executes after the request has gone and come back with a response
		 * error- executes when something goes wrong (like a 500 error perhaps)
		 */
		return FilterConstants.ROUTE_TYPE;
	}

	@Override
	public int filterOrder() {
		/*
		 * If you have MANY filters of the same type then you can set a
		 * precedence order to them.
		 */
		return 0;
	}

}
