package com.scorpionglitch.gmzuul.filters;

import javax.servlet.http.HttpServletRequest;

import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

public class PreFilter extends ZuulFilter {
	private String requestURI;
	private String requestMethod;
	
	@Override
	public boolean shouldFilter() {
		RequestContext context = RequestContext.getCurrentContext();
		HttpServletRequest request = context.getRequest();
		
		requestURI = request.getRequestURI();
		requestMethod = request.getMethod();
		
		return true;
	}

	@Override
	public Object run() throws ZuulException {
		System.out.println("PreFilter: (" + requestMethod + ") " + requestURI);
		
		return null;
	}

	@Override
	public String filterType() {
		return FilterConstants.PRE_TYPE;
	}

	@Override
	public int filterOrder() {
		return 0;
	}
}
